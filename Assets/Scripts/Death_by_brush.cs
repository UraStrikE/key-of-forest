﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death_by_brush : MonoBehaviour
{
    public Transform spawn;

    /// <summary>
    /// Проверка наличия игрока в области
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            StartCoroutine(Die(collision.gameObject));
        }
    }
    /// <summary>
    /// Воспроизведение смерти игрока
    /// </summary>
    /// <param name="player"></param>
    /// <returns></returns>
    IEnumerator Die(GameObject player)
    {
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        player.GetComponent<Player>().died = true;
        player.GetComponent<CharacterController2D>().animator.SetBool("Jump", false);
        player.GetComponent<CharacterController2D>().animator.SetBool("Die", true);
        yield return new WaitForSeconds(1.5f);
        player.transform.position = spawn.position;
        player.GetComponent<Player>().died = false;
        player.GetComponent<CharacterController2D>().animator.SetBool("Die", false);
        yield break;
        
    }
}
