﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;

public static class Save_system 
{
    /// <summary>
    /// Сохранение указанного уровня
    /// </summary>
    /// <param name="level"></param>
    public static void SaveProgress (int level)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + "/progress.qx";
        FileStream stream = new FileStream(path, FileMode.OpenOrCreate);

        formatter.Serialize(stream, level);
        stream.Close();
    }

    /// <summary>
    /// Загрузка достигнутого уровня
    /// </summary>
    /// <returns></returns>
    public static int? LoadProgress()
    {
        string path = Application.persistentDataPath + "/progress.qx";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.OpenOrCreate);

            int? level = formatter.Deserialize(stream) as int?;
            stream.Close();
            
            return level;
        }
        else
        {
            return null;
        }
    }
}
