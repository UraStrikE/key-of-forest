﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform_grow : MonoBehaviour
{
	[SerializeField] private GameObject platform;
	[SerializeField] private GameObject platform_projection;
	private GameObject platformExist;
	private GameObject projectionExist;
	public bool RightOrLeft; // left - false, right - true

	/// <summary>
	/// Создание платформы
	/// </summary>
	public void OnMouseDown()
	{
		if (!platformExist)
		{
			Destroy(projectionExist);
			if (RightOrLeft)
			{
				platformExist = Instantiate(platform, new Vector3(this.transform.position.x + 1.5f, this.transform.position.y, 1f), transform.rotation);
			}
			else
			{
				platformExist = Instantiate(platform, new Vector3(this.transform.position.x - 1.5f, this.transform.position.y, 1f), transform.rotation);
			}
		}
		else
		{
			platformExist.GetComponent<Platform>().Destroy_Platform();
		}
	}

	/// <summary>
	/// Создание проекции платформы
	/// </summary>
	public void OnMouseEnter()
	{
		if (!platformExist)
		{
			if (RightOrLeft)
			{
				projectionExist = Instantiate(platform_projection, new Vector3(this.transform.position.x + 1.5f, this.transform.position.y ), transform.rotation);
			}
			else
			{
				projectionExist = Instantiate(platform_projection, new Vector3(this.transform.position.x - 1.5f, this.transform.position.y ), transform.rotation);
			}
		}
	}

	/// <summary>
	/// Удаление проекции платформы
	/// </summary>
	public void OnMouseExit()
	{
		if (!platformExist)
		{
			Destroy(projectionExist);
		}
	}

}
