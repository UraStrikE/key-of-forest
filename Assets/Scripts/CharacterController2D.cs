using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private float m_RunSpeed = 40f;
    [SerializeField] private float m_JumpForce = 140f;                          
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  
    [SerializeField] private bool m_AirControl = false;                         
    [SerializeField] private LayerMask m_WhatIsGround;                          
    [SerializeField] private Transform m_GroundCheck;                           

    const float k_GroundedRadius = .1f;
    public bool m_Grounded;            
 
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true; 
    private Vector3 m_Velocity = Vector3.zero;

    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    public Animator animator;

    /// <summary>
    /// ���������� �����������
    /// </summary>
    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

        animator = gameObject.GetComponent<Animator>();
    }

    /// <summary>
    /// �������� ����������� ���������
    /// </summary>
    private void FixedUpdate()
    {
        bool wasGrounded = m_Grounded;
        m_Grounded = false;

        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                m_Grounded = true;                
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }
        }
    }

    /// <summary>
    /// �������������� ����������� ������
    /// </summary>
    /// <param name="move"></param>
    public void Move(float move)
    {
        animator.SetFloat("Move", Mathf.Abs(move)*Time.deltaTime);
        animator.SetBool("Jump", !m_Grounded);
        move = move * m_RunSpeed * Time.deltaTime;
     
        if (m_Grounded || m_AirControl)
        {
        
            Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody2D.velocity.y);         
            m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            if (move > 0 && !m_FacingRight)
            {
                Flip();
            }
            else if (move < 0 && m_FacingRight)
            {
                Flip();
            }
        }
    }

    /// <summary>
    /// ������ ������
    /// </summary>
    public void Jump()
    {
        if (m_Grounded)
        {
            m_Grounded = false;
            m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));            
        }
    }

    /// <summary>
    /// ������� ������� ������ ������������ ���
    /// </summary>
    private void Flip()
    {
        m_FacingRight = !m_FacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
