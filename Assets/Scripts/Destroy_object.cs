﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy_object : MonoBehaviour
{    
    public GameObject button;
    private bool? BPOrBJ;
    
    /// <summary>
    /// Определение принадлежности кнопки к типу Объект или Игрок
    /// </summary>
    private void Start()
    {
        if (button.GetComponent<Button_player>() != null)
        {
            BPOrBJ = true;
        }
        else if (button.GetComponent<Button_object>() != null)
        {
            BPOrBJ = false;
        }
        else
        {
            BPOrBJ = null;
        }
    }

    /// <summary>
    /// Уничтожения объекта
    /// </summary>
    void Update()
    {
        if (BPOrBJ == true)
        {
            if (button.GetComponent<Button_player>().Active)
            {
                Destroy(gameObject);
            }
        }
        else if (BPOrBJ == false)
        {
            if (button.GetComponent<Button_object>().Active)
            {
                Destroy(gameObject);
            }
        }
    }
}
