﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Main_menu : MonoBehaviour
{
    public GameObject MenuPanel;
    public GameObject SettingPanel;
    public Slider MusicSlide;
    public Slider SoundSlide;

    /// <summary>
    /// Загрузка последней достигнутой игроком игровой сцены
    /// </summary>
    public void Play()
    {
        Attribute.MusicVolume = MusicSlide.value;
        Attribute.SoundVolume = SoundSlide.value;
        if (Save_system.LoadProgress() == null || Save_system.LoadProgress() == 1)
        {
            SceneManager.LoadScene("Level1");
            Save_system.SaveProgress(1);
        }
        if (Save_system.LoadProgress() == 2)
        {
            SceneManager.LoadScene("Level2");
        }
        if (Save_system.LoadProgress() == 3)
        {
            SceneManager.LoadScene("Level3");
        }
        if (Save_system.LoadProgress() == 4)
        {
            SceneManager.LoadScene("Level4");
        }
        if (Save_system.LoadProgress() == 5)
        {
            SceneManager.LoadScene("Level5");
        }
        if (Save_system.LoadProgress() == 6)
        {
            SceneManager.LoadScene("Level6");
        }
    }

    /// <summary>
    /// Смена панели на настройки
    /// </summary>
    public void Settings()
    {
        MenuPanel.SetActive(false);
        SettingPanel.SetActive(true);
    }

    /// <summary>
    /// Выход из приложения
    /// </summary>
    public void Exit()
    {
        Application.Quit();
    }

    /// <summary>
    /// Обнуления прогресса
    /// </summary>
    public void ResetProgress()
    {
        Save_system.SaveProgress(1);
    }

    /// <summary>
    /// Изменение громкости музыки
    /// </summary>
    public void MusicChange()
    {
        Attribute.MusicVolume = MusicSlide.value;
    }

    /// <summary>
    /// Изменение громкости звука
    /// </summary>
    public void SoundChange()
    {
        Attribute.SoundVolume = SoundSlide.value;
    }

    /// <summary>
    /// Возврат на панель главного меню
    /// </summary>
    public void Back()
    {
        MenuPanel.SetActive(true);
        SettingPanel.SetActive(false);
    }

}
