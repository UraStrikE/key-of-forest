﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotat_object : MonoBehaviour
{
    public GameObject button;
    private bool? BPOrBJ;
    private bool activeted = true;

    /// <summary>
    /// Определение принадлежности кнопки к типу Объект или Игрок
    /// </summary>
    private void Start()
    {
        if (button.GetComponent<Button_player>() != null)
        {
            BPOrBJ = true;
        }
        else if (button.GetComponent<Button_object>() != null)
        {
            BPOrBJ = false;
        }
        else
        {
            BPOrBJ = null;
        }
    }

    /// <summary>
    /// Активация поворота объекта кнопкой
    /// </summary>
    void Update()
    {
        if (activeted)
        {
            if (BPOrBJ == true)
            {
                if (button.GetComponent<Button_player>().Active)
                {
                    StartCoroutine(rotate(-80.0f, 5.0f));
                    activeted = false;
                }
            }
            else if (BPOrBJ == false)
            {
                if (button.GetComponent<Button_object>().Active)
                {
                    StartCoroutine(rotate(-80.0f, 5.0f));
                    activeted = false;
                }
            }
        }
    }

    /// <summary>
    /// Поворот объекта 
    /// </summary>
    /// <param name="angle"></param>
    /// <param name="intensity"></param>
    /// <returns></returns>
    IEnumerator rotate(float angle, float intensity)
    {
        var me = gameObject.transform;
        var to = me.rotation * Quaternion.Euler(0.0f, 0.0f, angle);

        while (true)
        {
            me.rotation = Quaternion.Lerp(me.rotation, to, intensity * Time.deltaTime);

            if (Quaternion.Angle(me.rotation, to) < 0.01f)
            {
                me.rotation = to;
                yield break;
            }

            yield return null;
        }
    }
}
