﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall_grow : MonoBehaviour
{
	[SerializeField] private GameObject wall;
	[SerializeField] private GameObject wall_projection;
	private GameObject wallExist;
	private GameObject wall_projectionExist;

	/// <summary>
	/// Создание стены
	/// </summary>
	public void OnMouseDown()
	{
		if (!wallExist)
		{
			Destroy(wall_projectionExist);
			wallExist = Instantiate(wall, new Vector3(this.transform.position.x, this.transform.position.y + 1.5f, 1f), transform.rotation);
		}
		else
		{			
			wallExist.GetComponent<Wall>().Destroy_Wall();
		}
	}

	/// <summary>
	/// Создание проекции стены
	/// </summary>
	public void OnMouseEnter()
	{
		if (!wallExist)
		{
			wall_projectionExist = Instantiate(wall_projection, new Vector3(this.transform.position.x, this.transform.position.y + 1.5f), transform.rotation);
		}
	}

	/// <summary>
	/// Удаление проекции стены
	/// </summary>
	public void OnMouseExit()
	{
		if (!wallExist)
		{
			Destroy(wall_projectionExist);
		}
	}
}
