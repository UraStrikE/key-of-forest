﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [Range(0f, 0.1f)] [SerializeField] private float grow_str = 0.1f;
    [Range(0f, 0.1f)] [SerializeField] private float grow_to = 0.0001f;
    BoxCollider2D collider;
    Animator animator;

    /// <summary>
    /// Проверка наличия стены и её создания
    /// </summary>
    void Start()
    {
        if (Attribute.Wall != null)
        {
            Attribute.Wall.GetComponent<Wall>().Destroy_Wall();
        }
        collider = GetComponent<BoxCollider2D>();
        collider.size = new Vector2(0f, 2f);
        collider.offset = new Vector2(0f, -1f);

        StartCoroutine(grow());

        animator = GetComponent<Animator>();
        animator.SetBool("start", true);
        Attribute.Wall = gameObject;
    }

    /// <summary>
    /// Уничтожение стены игроком
    /// </summary>
    public void Destroy_Wall()
    {
        animator.SetBool("destroy", true);
        StartCoroutine(destroy());
    }

    /// <summary>
    /// Изменение коллайдера при создании 
    /// </summary>
    /// <returns></returns>
    IEnumerator grow()
    {
        float offsetY = -1f;
        for (float i = 0f; i < 2f; i += grow_str)
        {
            collider.size = new Vector2(0.875f, i);
            offsetY = -1f + i / 2;
            collider.offset = new Vector2(0f, offsetY);
            yield return new WaitForSeconds(grow_to);
        }
        collider.size = new Vector2(0.875f, 2f);
        yield break;
    }

    /// <summary>
    /// Уничтожение объекта
    /// </summary>
    /// <returns></returns>
    IEnumerator destroy()
    {
        collider.size = new Vector2(0f, 0f);
        yield return new WaitForSeconds(1.2f);
        Destroy(this.gameObject);
        yield break;
    }
}
