﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_object : MonoBehaviour
{
    public Transform toPoint;
    public GameObject KeyObject;
    private GameObject now_Key;
    public Button_player button;
    private bool activeted = true;
    
    /// <summary>
    /// Создание оюъекта по нажатию кнопки
    /// </summary>
    void Update()
    {
        if (button.Active && activeted)
        {
            if (now_Key != null)
            {
                Destroy(now_Key);
            }
            now_Key = Instantiate(KeyObject, toPoint.position, Quaternion.identity);
            activeted = false;
            StartCoroutine(change());
        }
    }

    /// <summary>
    /// Задержка создания объекта
    /// </summary>
    /// <returns></returns>
    IEnumerator change()
    {
        yield return new WaitForSeconds(3f);
        activeted = true;
    }
}
