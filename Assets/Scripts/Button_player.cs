﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_player : MonoBehaviour
{
    public bool Active = false;
    public Transform button;
    public float range = 0.1f;

    /// <summary>
    /// Измена состояния кнопки при попадания игрока в область
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Active = true;
            button.position = new Vector2(button.position.x, button.position.y - range);
        }        
    }

    /// <summary>
    /// Измена состояния кнопки при покидания игрока из области
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Active = false;
            button.position = new Vector2(button.position.x, button.position.y + range);
        }
    }
}
