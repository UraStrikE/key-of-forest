﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [Range(0f, 0.1f)] [SerializeField] private float grow_str = 0.1f;
    [Range(0f, 0.1f)] [SerializeField] private float grow_to = 0.0001f;
    private BoxCollider2D collider;
    private Animator animator;
    public bool RightOrLeft;

    /// <summary>
    /// Проверка наличия платформы и её создания
    /// </summary>
    void Start()
    {
        if (Attribute.Platform != null)
        {
            Attribute.Platform.GetComponent<Platform>().Destroy_Platform();
        }
        collider = GetComponent<BoxCollider2D>();
        collider.size = new Vector2(0f, 0.1f);
        if (RightOrLeft)
        {
            collider.offset = new Vector2(-1f, 0.32f);
        }
        else
        {
            collider.offset = new Vector2(1f,0.32f);
        }

        animator = GetComponent<Animator>();
        animator.SetBool("start", true);
        StartCoroutine(grow());
        Attribute.Platform = gameObject;
    }

    /// <summary>
    /// Уничтожение платформы игроком
    /// </summary>
    public void Destroy_Platform()
    {
        animator.SetBool("destroy", true);
        StartCoroutine(destroy());
    }

    /// <summary>
    /// Изменение коллайдера при создании 
    /// </summary>
    /// <returns></returns>
    IEnumerator grow()
    {
        if (RightOrLeft)
        {
            float offsetY = -1f;
            for (float i = 0f; i <= 2f; i += grow_str)
            {
                collider.size = new Vector2(i, 0.1f);
                offsetY = -1f + i / 2;
                collider.offset = new Vector2(offsetY, 0.32f);
                yield return new WaitForSeconds(grow_to);
            }
            collider.offset = new Vector2(0f, 0.32f);
            collider.size = new Vector2(2f, 0.1f);
        }
        else
        {
            float offsetY = 1f;
            for (float i = 0f; i <= 2f; i += grow_str)
            {
                collider.size = new Vector2(i, 0.1f);
                offsetY = 1f - i / 2;
                collider.offset = new Vector2(offsetY, 0.32f);
                yield return new WaitForSeconds(grow_to);
            }
            collider.offset = new Vector2(0f, 0.32f);
            collider.size = new Vector2(2f, 0.1f);
        }       
        yield break;
    }

    /// <summary>
    /// Уничтожение объекта
    /// </summary>
    /// <returns></returns>
    IEnumerator destroy()
    {
        yield return new WaitForSeconds(0.8f);
        Destroy(collider);
        yield return new WaitForSeconds(0.2f);
        Destroy(this.gameObject);
        yield break;
    }
}
