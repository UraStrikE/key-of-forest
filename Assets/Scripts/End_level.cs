﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class End_level : MonoBehaviour
{
    public int LevelNext;

    /// <summary>
    /// Загрузка указанного уровня
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            if (LevelNext == 1)
            {
                SceneManager.LoadScene("Level1");
            }
            if (LevelNext == 2)
            {
                SceneManager.LoadScene("Level2");
            }
            if (LevelNext == 3)
            {
                SceneManager.LoadScene("Level3");
            }
            if (LevelNext == 4)
            {
                SceneManager.LoadScene("Level4");
            }
            if(LevelNext == 5)
            {
                SceneManager.LoadScene("Level5");
            }
            if(LevelNext == 6)
            {
                SceneManager.LoadScene("Level6");
            }
            if (LevelNext == 0)
            {
                SceneManager.LoadScene("Main_menu");
            }

            if (LevelNext > 0)
            {
                Save_system.SaveProgress(LevelNext);
            }
        }
    }
}
