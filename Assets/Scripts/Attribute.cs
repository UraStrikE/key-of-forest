﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Attribute
{
    public static GameObject Wall { get; set; }

    public static GameObject Platform { get; set; }

    public static float MusicVolume { get; set; }

    public static float SoundVolume { get; set; }
}
