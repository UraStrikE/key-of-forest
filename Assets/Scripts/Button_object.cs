﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button_object : MonoBehaviour
{
    public bool Active = false;

    /// <summary>
    /// Измена состояния кнопки при попадания ключегого объекта
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Key_object")
        {
            Active = true;
            collision.gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        }
    }
}
