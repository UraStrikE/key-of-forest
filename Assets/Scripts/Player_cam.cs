﻿using UnityEngine;
using System.Collections;
using System;

public class Player_cam : MonoBehaviour
{
	public float damping = 1.5f;
	private Transform player;
	public AudioSource Music, Sound;

	/// <summary>
	/// Нахождение игрока и установка громкости музыки и звука
	/// </summary>
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		Music.volume = Attribute.MusicVolume;
		Sound.volume = Attribute.SoundVolume;

		Music.Play();
		Sound.Play();
	}

	/// <summary>
	/// Преследование камеры за игрококм
	/// </summary>
	void Update()
	{
		if (player)
		{
			Vector3 target;
			target = new Vector3(player.position.x, player.position.y, transform.position.z);
			Vector3 currentPosition = Vector3.Lerp(transform.position, target, damping * Time.deltaTime);
			transform.position = currentPosition;
		}
	}
}