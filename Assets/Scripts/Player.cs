﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public CharacterController2D Controller_Character;

    private float direction = 0f;
    public bool jump = false,
         died = false;

    /// <summary>
    /// Учет нажатия кнопок
    /// </summary>
    void FixedUpdate()
    {
        direction = Input.GetAxisRaw("Horizontal");
        if (!died)
        {
            if (Input.GetButton("Jump"))
            {
                Controller_Character.Jump();
            }
            Controller_Character.Move(direction);
        }

        if (Input.GetButtonDown("Escape"))
        {
            ExitToMenu();
        }
    }

    /// <summary>
    /// Выход в главное меню
    /// </summary>
    void ExitToMenu()
    {
        SceneManager.LoadScene("Main_menu");
    }
}
